var installMgBlock = function(selector, config) {

    var slot = $(selector)

    var layout = config.layout
    $.get(layout,function(data) {
        slot.append(data)

        $(config.match).each(function(){
            var url = this.mgblock;
            var region = this.region;
            $.get(url,function(data) {
                slot.find(region).append(data);
            });
        })
    });
};

// var config =
//     {
//         layout: 'http://view-layout-provider.local/api/view-layout/__base__3_2R',
//
//         match:  [
//             {
//                 region: 'region-46de5ec4f-left',
//                 mgblock: 'http://view-layout-provider.local/api/mgblock/_example_simple_block'
//             },
//             {
//                 region: 'region-7f775a7fd-bottom',
//                 mgblock: 'http://view-layout-provider.local/api/mgblock/_example_simple_block2'
//             }
//         ]
//     }
//
// installMgBlock('#box', config);
