(function() {

    var mgblocks = $('div[mgblock]')

    mgblocks.each(function () {
        // console.log($(this).attr('mgblock'));
        var mgblockName = $(this).attr('mgblock');
        var mgblockHTML = $(this).attr('mgblock-html');
        var mgblockJS = $(this).attr('mgblock-js');
        var mgblockCSS = $(this).attr('mgblock-css');
        // console.log(mgblockName, mgblockJS, mgblockCSS);


        /**
         * Todo: API从InfoCenter自动获取
         */

        var that = this
        var url = 'http://view-layout-provider.local/api/mgblock/' + mgblockName + '/resource/' + mgblockHTML;
        $.get(url, function(data){
            $(that).html('\
                ' + data + '\
                <link rel="stylesheet" href="http://view-layout-provider.local/api/mgblock/' + mgblockName + '/resource/' + mgblockCSS + '" />\
                <script src="http://view-layout-provider.local/api/mgblock/' + mgblockName + '/resource/' + mgblockJS + '"></script>\
            ');
        })

        var randStr = Math.random().toString(36).replace(/[^a-z0-9]+/g, '').substr(0, 9);
        // console.log(33333, randStr)
        $(this).parent().attr('id', 'mgb-w-' + randStr)

        $(this).removeAttr('mgblock');
        $(this).removeAttr('mgblock-html');
        $(this).removeAttr('mgblock-js');
        $(this).removeAttr('mgblock-css');
    })

}());
