
var loadJQuery = function() {
    var url = 'https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js'
    var script = document.createElement("SCRIPT")
    script.src = url
    script.type = 'text/javascript'
    script.onload = function() {
        window.JQUERY_LOADED = true
    }

    if ( (typeof window.JQUERY_LOADED === 'undefined') ||
         (window.JQUERY_LOADED === false)
    ) {
        document.getElementsByTagName('div')[0].appendChild(script);
    }
}

var loadBasic = function() {
    var url = 'http://view-layout-provider.local/api/mgblock/000_common/resource/basic.js'
    var script = document.createElement("SCRIPT")
    script.src = url
    script.type = 'text/javascript'
    script.onload = function() {
        window.BASIC_LOADED = true
    }

    if ( (typeof window.BASIC_LOADED === 'undefined') ||
        (window.BASIC_LOADED === false)
    ) {
        document.getElementsByTagName('div')[0].appendChild(script);
    }
}

var loadBasic02 = function() {
    var url = 'http://view-layout-provider.local/api/mgblock/000_common/resource/basic02.js'
    var script = document.createElement("SCRIPT")
    script.src = url
    script.type = 'text/javascript'
    script.onload = function() {
        window.BASIC02_LOADED = true
    }

    if ( (typeof window.BASIC02_LOADED === 'undefined') ||
        (window.BASIC02_LOADED === false)
    ) {
        document.getElementsByTagName('div')[0].appendChild(script);
    }
}
