<?php

namespace App\Controller\Api;

require __DIR__ . '/../../../_lib_/uuid.php';

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ViewLayoutApiController extends AbstractController
{
    const REAL_DATA_DIR = 'real';

    /**
     * Todo: [< TestCase >]
     *    http://view-layout-provider.local/api/view-layout/__base__3_2R
     *    http://view-layout-provider.local/api/view-layout/_example__3_2R_with_mgblocks
     *    动态内容加载
     */

    /**
     * @Route("/api/view-layout/{layout_name}", name="api_view_layout")
     */
    public function viewLayout(string $layout_name): Response
    {
        $targetDir = '/temp-data/layouts/' . $layout_name;
        $html = file_get_contents(__DIR__ . $targetDir . '/real/index.html');

        return new Response($html);
    }

    /**
     *
     * Todo: 登记该API到 INfoCenter
     *
     * Todo: [< TestCase >]
     *     http://view-layout-provider.local/api/mgblock/_example_simple_block
     *
     */
    /**
     * @Route("/api/mgblock/{mgblock_name}", name="api_mgblock")
     */
    public function mgBlock(string $mgblock_name): Response
    {
        $targetDir = '/temp-data/mgblock/' . $mgblock_name;
        $html = file_get_contents(__DIR__ . $targetDir . '/facade.html');

        return new Response($html);
    }


    /**
     *
     * Todo: 登记该API到 INfoCenter
     *
     */
    /**
     * @Route("/api/mgblock/{mgblock_name}/resource/{resource_name}", name="api_mgblock_resource")
     */
    public function mgBlockResource(string $mgblock_name, $resource_name, Request $req): Response
    {
        $targetBaseDir = '/temp-data/mgblock/' . $mgblock_name . '/' . self::REAL_DATA_DIR;
        if ( strpos($resource_name, '.js') === (strlen($resource_name)-3) ) {
            $typeDir = 'js';
        }elseif ( strpos($resource_name, '.css')  === (strlen($resource_name)-4) ) {
            $typeDir = 'css';
        }elseif (strpos($resource_name, '.html')  === (strlen($resource_name)-5) ) {
            $typeDir = '';
        }
        $targetDir = $targetBaseDir . '/' . $typeDir;
        $resource = file_get_contents(__DIR__ . $targetDir . '/' . $resource_name);

        $res = new Response($resource);
        if ($typeDir === 'js') {
            $res->headers->set('Content-Type', 'text/javascript');
        }
        if ($typeDir === 'css') {
            $res->headers->set('Content-Type', 'text/css');
        }

        return $res;
    }


    /**
     * @Route("/api/view-layout-catalog", name="api_view_layout_catalog")
     */
    public function getViewLayoutCatalog(): Response
    {
        return new JsonResponse([]);
    }
}
