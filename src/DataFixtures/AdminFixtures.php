<?php

namespace App\DataFixtures;

use App\Entity\Admin;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdminFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * AdminFixtures000 constructor.
     *
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $adminValues = [
            [
                'email'         => 'admin@example.com',
                'plainPassword' => 'admin@example.com',
                'roles'         => [Admin::USER_ROLE_ADMIN],
            ],
        ];

        foreach ($adminValues as $key => $values) {
            $admin = new Admin();
            $admin->setEmail($values['email']);
            $admin->setRoles($values['roles']);
            $admin->setPassword($this->passwordEncoder->encodePassword($admin, $values['plainPassword']));

            $manager->persist($admin);

            $this->addReference('admin' . $key, $admin);
        }

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 0;
    }

}
