<?php

const DEFAULT_LENGTH = 9;

function uuid(int $length): string
{
    $id = md5(uniqid(mt_rand(), true));

    if ($length === 0) {
        return substr($id, 0, DEFAULT_LENGTH);
    }

    if ($length < 0) {
        return $id;
    }

    return substr($id, 0, $length);
}
